from collections import OrderedDict
from math import log
def readfq(my_file=None,limit=None,scale=-33):

    ''' This is a function to read fastq files. It returns the out as a dictionary files 
        Unlike the biopython module this returns the error probabilities
        and accuracy . All in percentages
        >> from fq_parser import readfq
            data = read_fq(my_file =file, limit=None)

            first_data=data[1]
            sequence=first_data['sequence']
            name=first_data['name']
            id=first_data['id']
            scores_ascii=first_data['quality score ascii']
            raw_scores=first_data['quality score raw']
            error_probatility=first_data['error']
            accuracy=first_data['accuracy']

            
         my_file argument accepts the path to the file
         limit used to set how many records you want to read
         in case the file contains more than one record
         limit of 10 means first 10 records should be read
          by default None returns a limit of 1
          
         fetch them by using the dictionary keys from numbes 1 to the limit used

         by default it uses the sanger encoding which is 33
         
        
         Any contributions can be sent to author
         Vincent Appiah   email:  appiahv@rocketmail.com'''
        
    if my_file ==None:
        return('empty')
    
    else:
        
        if limit==None:
            limit=1
        else:
            if 'int' not in str(type(limit)):
                return('limit should be an integer')
            
            
        
        my_data=open(my_file,'r')
        items=''
        arr=[]
        new=OrderedDict()
        my_dict=OrderedDict()
        initial=1
        counter=1
        for line in my_data:
            
            if len(my_dict)==limit:
                break
            
            data=line.strip()
            if data[0]=='@':
            #print arr
                if len(new)!=0:
                 
                    
                    new['quality score raw']=tuple([str(qualityscore(i,scale)) for i in new['quality score ascii']])
                   

                    
                    new['error']=tuple([ '%0.3f'% ((10**(-float(i)/10.0))*100) for i in new['quality score raw']])
                   
                    new['accuracy']=tuple([ '%0.3f'% (100-((10**(-float(i)/10.0))*100)) for i in new['quality score raw']])

                    
                    my_dict[initial]={}
                    temp=my_dict[initial]
                    for key,value in new.items():
                        temp[key]=value
                    my_dict[initial]=temp
                    
                    
                    initial+=1

                

                    
            
                    new.clear()
                    counter=1
                   

                    new['name']=data
                    counter+=1
                else:
                    new['name']=data
                    counter+=1
        
            else:
                if counter==2:
                    new['sequence']=data
                    
                    counter+=1
                elif counter==3:
                    new['other info']=data
                   
                    counter+=1
                elif counter==4:
                    new['quality score ascii']=data
                    counter+=1
                   
        if limit==1:
            my_dict=my_dict[1]
        return(my_dict)

def qualityscore(seq,scale=-33):
    
    ''' Find the Quality Scores'''
    
    q=ord(seq)+scale
    return(q)

